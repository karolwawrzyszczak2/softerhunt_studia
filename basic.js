class Kwadrat{ 

    constructor(bok){
        this.bok = bok;
    }
    get getBok(){
        return this.bok;
    }
    set setBok(wartosc){
        this.bok = wartosc;
    }
    pole(){
        return this.bok * this.bok;
    }
    obwod(){
        return this.bok * 4;
    }
    
}

class Prostokat{ 

    constructor(bokA,bokB){
        this.bokA = bokA;
        this.bokB = bokB;
    }
    get getBokA(){
        return this.bokA;
    }
    get getBokB(){
        return this.bokB;
    }
    set setBokA(wartosc){
        this.bokA = wartosc;
    }
    set setBokB(wartosc){
        this.bokB = wartosc;
    }
    pole(){
        return this.bokA * this.bokB;
    }
    obwod(){
        return this.bokA*2 + this.bokB*2;
    }
    
}

class Trojkat extends Kwadrat{ 
    constructor(bok,wysokosc){
        super(bok);
        this.wysokosc = wysokosc;
    }
    get getWysokosc(){
        return this.wysokosc;
    }
    set setWysokosc(wartosc){
        this.wysokosc = wartosc;
    }
    trianglepole(){
        return (this.bok*this.wysokosc)/2;
    }
    triangleobwod(){
        return this.bok*3;
    }
    
    
}
let zxc = new Kwadrat(3);
zxc.bok = 4;
console.log(zxc.bok);